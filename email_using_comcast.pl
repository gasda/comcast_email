#!/usr/bin/perl
#
use strict;
use Net::SMTP::SSL;
use MIME::Base64;

my $USER_NAME = 'mycomcastaccount@comcast.net';
my $USER_PASS = 'mycomcastpassword';

my $smtp = Net::SMTP::SSL->new(
    "smtp.comcast.net",
    Hello => "host.comcast.net",
    Port => 465,
    Timeout => 30,
    Debug => 1,
);

$smtp->datasend("AUTH LOGIN\n");
$smtp->response();

$smtp->datasend(encode_base64($USER_NAME));
$smtp->response();
$smtp->datasend(encode_base64($USER_PASS));
$smtp->response();

# Email from
$smtp->mail($USER_NAME)

# Email to
my $to = 'myotheremail@gmail.com';
$smtp->to($to);

$smtp->data();

my $email_body = <<"EOF";
To: $to
From: $USER_NAME
Subject: Text EMail from comcast account.

Well this seems to work just fine.

EOF

$smtp->datasend("$email_body");
$smtp->dataend();
$smtp->quit();
exit;
